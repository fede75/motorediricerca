
<?php
require '../../../vendor/autoload.php';
$client = Elasticsearch\ClientBuilder::create()->build();
 
$params = [
	'index' => 'phaidra',
	'type' => 'collections',
	'id' => '307311',
];
 
$response = $client->get($params);
echo $response['_source']['collection'];
