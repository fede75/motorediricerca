<?php



$m = new MongoClient('mongodb://fede75:Vallisneri_1661@ds143330.mlab.com:43330/phaidra');
$db = $m->selectDB("phaidra");
	
$select=$_POST['select'];
$string=$_POST['nome'];


switch ($select) {
	
	case "collections2":
	
	
	
$collection = $db->selectCollection('collections2');




$cursor = $collection->find(
    array('$text' => array('$search' => $string)),
    array('score' => array('$meta' => 'textScore'))
);

$cursor = $cursor->sort(
    array('score' => array('$meta' => 'textScore'))
);


echo "<pre>";
echo "Elenco document:<br />";
echo "<table>";










foreach($cursor as $document) {
	
	echo "<tr>";
	echo "<td>Collezione:" . $document['pid2'] . '<img src="http://fc.cab.unipd.it/fedora/objects/o:' . $document['pid2'] . '/methods/bdef:Asset/getThumbnail  ">';
	

	
/// splitter :-?  ////////////////////////	
$riga = '{"metadata":{"uwmetadata":[{"children":[{"datatype":"CharacterString","input_type":"input_text","ui_value":"o:61439","xmlname":"identifier","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"select","ui_value":"de","xmlname":"lang"}],"datatype":"LangString","input_type":"input_text","ui_value":"Echte Ananas (Ananassa sativa Lind.) - Amerikanische Agave (Agave americana L.)","xmlname":"title","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"datatype":"Language","input_type":"select","ui_value":"de","xmlname":"language","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"select","ui_value":"de","xmlname":"lang"}],"datatype":"LangString","input_type":"input_text","ui_value":"Ausländische Kulturpflanzen in farbigen Wandtafeln. Abteilung II. Tafel 9. Fig.I: pianta intera e fiore. Fig.II: pianta intera e fiore.","xmlname":"description","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"select","ui_value":"it","xmlname":"lang"}],"datatype":"LangString","input_type":"input_text","ui_value":"Botanica, ananas, agave","xmlname":"keyword","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"select","ui_value":"it","xmlname":"lang"}],"datatype":"LangString","input_type":"input_text","ui_value":"Fine Ottocento-primi Novecento","xmlname":"coverage","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"datatype":"Boolean","input_type":"select","ui_value":"no","xmlname":"irdata","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/extended\/V1.0"},{"datatype":"Node","input_type":"node","xmlname":"identifiers","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/extended\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"general","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"children":[{"datatype":"DateTime","input_type":"input_text","ui_value":"2015-07-06T14:36:57.133Z","xmlname":"upload_date","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"datatype":"Vocabulary","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/voc_2\/44","xmlname":"status","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"datatype":"Boolean","input_type":"select","ui_value":"no","xmlname":"peer_reviewed","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/extended\/V1.0"},{"attributes":[{"input_type":"input_text","ui_value":"0","xmlname":"data_order"}],"children":[{"datatype":"Vocabulary","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/voc_3\/1552095","xmlname":"role","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"input_text","ui_value":"0","xmlname":"data_order"}],"children":[{"datatype":"CharacterString","input_type":"input_text","ui_value":"H.","xmlname":"firstname","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"},{"datatype":"CharacterString","input_type":"input_text","ui_value":"Zippel","xmlname":"lastname","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"}],"datatype":"Node","input_type":"node","xmlname":"entity","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"input_text","ui_value":"1","xmlname":"data_order"}],"children":[{"datatype":"CharacterString","input_type":"input_text","ui_value":"C.","xmlname":"firstname","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"},{"datatype":"CharacterString","input_type":"input_text","ui_value":"Bollmann","xmlname":"lastname","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"},{"datatype":"CharacterString","input_type":"input_text","ui_value":"person","xmlname":"type","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"}],"datatype":"Node","input_type":"node","xmlname":"entity","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"contribute","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"input_text","ui_value":"1","xmlname":"data_order"}],"children":[{"datatype":"Vocabulary","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/voc_3\/1552154","xmlname":"role","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"input_text","ui_value":"0","xmlname":"data_order"}],"children":[{"datatype":"CharacterString","input_type":"input_text","ui_value":"Michele","xmlname":"firstname","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"},{"datatype":"CharacterString","input_type":"input_text","ui_value":"Barollo","xmlname":"lastname","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"}],"datatype":"Node","input_type":"node","xmlname":"entity","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"input_text","ui_value":"1","xmlname":"data_order"}],"children":[{"datatype":"CharacterString","input_type":"input_text","ui_value":"Simone","xmlname":"firstname","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"},{"datatype":"CharacterString","input_type":"input_text","ui_value":"Citon","xmlname":"lastname","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"},{"datatype":"CharacterString","input_type":"input_text","ui_value":"person","xmlname":"type","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"}],"datatype":"Node","input_type":"node","xmlname":"entity","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"contribute","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"input_text","ui_value":"2","xmlname":"data_order"}],"children":[{"datatype":"Vocabulary","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/voc_3\/47","xmlname":"role","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"attributes":[{"input_type":"input_text","ui_value":"0","xmlname":"data_order"}],"children":[{"datatype":"CharacterString","input_type":"input_text","ui_value":"Friedrich Vieweg & Sohn","xmlname":"institution","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"},{"datatype":"CharacterString","input_type":"input_text","ui_value":"institution","xmlname":"type","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/entity"}],"datatype":"Node","input_type":"node","xmlname":"entity","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"contribute","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"lifecycle","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"children":[{"datatype":"CharacterString","input_type":"input_text","ui_value":"image\/jpeg","xmlname":"format","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"datatype":"FileSize","input_type":"input_text","ui_value":"1668337","xmlname":"size","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"datatype":"CharacterString","input_type":"input_text","ui_value":"http:\/\/phaidra.cab.unipd.it\/o:61439","xmlname":"location","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"children":[{"datatype":"Node","input_type":"node","xmlname":"orcomposite","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"requirement","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"technical","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"children":[{"children":[{"attributes":[{"input_type":"input_text","ui_value":"0","xmlname":"data_order"}],"datatype":"Vocabulary","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/educational\/voc_11\/1696","xmlname":"learningresourcetype","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/educational"}],"datatype":"Node","input_type":"node","xmlname":"educationals","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"educational","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"children":[{"datatype":"Boolean","input_type":"select","ui_value":"no","xmlname":"cost","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"datatype":"Boolean","input_type":"select","ui_value":"yes","xmlname":"copyright","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"datatype":"License","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/voc_21\/1","xmlname":"license","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"rights","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"children":[{"children":[{"datatype":"Node","input_type":"node","xmlname":"entity","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/annotation"}],"datatype":"Node","input_type":"node","xmlname":"annotations","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/annotation"}],"datatype":"Node","input_type":"node","xmlname":"annotation","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"children":[{"datatype":"Vocabulary","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/voc_6\/70","xmlname":"purpose","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"classification","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"children":[{"children":[{"datatype":"Faculty","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/organization\/voc_faculty\/UNIPDMUSEI","xmlname":"faculty","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/organization"},{"datatype":"Department","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/organization\/voc_department\/MuseoBotanico","xmlname":"department","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/organization"}],"datatype":"Node","input_type":"node","xmlname":"orgassignment","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/organization"},{"datatype":"Node","input_type":"node","xmlname":"curriculum","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0\/organization"}],"datatype":"Node","input_type":"node","xmlname":"organization","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/lom\/V1.0"},{"children":[{"attributes":[{"input_type":"select","ui_value":"it","xmlname":"lang"}],"datatype":"LangString","input_type":"input_text","ui_value":"Pulito nel 2014","xmlname":"inscription","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/histkult\/V1.0"},{"attributes":[{"input_type":"input_text","ui_value":"0","xmlname":"data_order"}],"children":[{"datatype":"Vocabulary","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/histkult\/V1.0\/voc_24\/1557151","xmlname":"resource","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/histkult\/V1.0"},{"datatype":"Vocabulary","input_type":"select","ui_value":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/histkult\/V1.0\/voc_22\/10880","xmlname":"dimension_unit","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/histkult\/V1.0"},{"datatype":"CharacterString","input_type":"input_text","ui_value":"50","xmlname":"width","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/histkult\/V1.0"},{"datatype":"CharacterString","input_type":"input_text","ui_value":"70","xmlname":"height","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/histkult\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"dimensions","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/histkult\/V1.0"},{"datatype":"Node","input_type":"node","xmlname":"reference_number","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/histkult\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"histkult","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/histkult\/V1.0"},{"children":[{"children":[{"attributes":[{"input_type":"input_text","ui_value":"0","xmlname":"data_order"}],"datatype":"Node","input_type":"node","xmlname":"entity","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/provenience\/V1.0"},{"attributes":[{"input_type":"select","ui_value":"it","xmlname":"lang"}],"datatype":"LangString","input_type":"input_text","ui_value":"Museo Botanico, cassettiera","xmlname":"location","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/provenience\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"contribute","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/provenience\/V1.0"}],"datatype":"Node","input_type":"node","xmlname":"provenience","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/provenience\/V1.0"},{"datatype":"Node","input_type":"node","xmlname":"digitalbook","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/digitalbook\/V1.0"},{"datatype":"Node","input_type":"node","xmlname":"etheses","xmlns":"http:\/\/phaidra.univie.ac.at\/XML\/metadata\/etheses\/V1.0"}]}}';
$inizio = strpos($riga, 'CharacterString","input_type":"input_text","ui_value":"o:');
$nuova_riga = substr($riga, $inizio);
$rest = substr($nuova_riga, 57, 5);
echo $rest;
/////////////////////////////////////////
	
	
	
	
	
	
	
	
	
	
	echo "<br>";
	
	echo "<td>Oggetto:"."<td>" . $document['res2'] . "<td>".'<img src="http://fc.cab.unipd.it/fedora/objects/o:' . $rest . '/methods/bdef:Asset/getThumbnail  ">';

	
	
	
	
echo "</tr>";
}
echo "</table>";
echo "</pre>";

break;

case "collections":
	



$collection2 = $db->selectCollection("collections");




$cursor2 = $collection2->find(
    array('$text' => array('$search' => $string)),
    array('score' => array('$meta' => 'textScore'))
);

$cursor2 = $cursor2->sort(
    array('score' => array('$meta' => 'textScore'))
);


echo "<pre>";
echo "Elenco document:<br />";
echo "<table>";










foreach($cursor2 as $document2) {
	
	echo "<tr>";
	echo "<td>Collezione:" . $document2['pid'] . '<img src="http://fc.cab.unipd.it/fedora/objects/o:' . $document2['pid'] . '/methods/bdef:Asset/getThumbnail  ">';
	
	$riga2=$document2['res'];
$inizio2 = strpos($riga2, 'http://phaidra.cab.unipd.it/o:');



$nuova_riga2 = substr($riga2, $inizio2);
$rest2= substr($nuova_riga2, 30, 5);
	
	
	
	
	
	//echo '<div><img src="http://fc.cab.unipd.it/fedora/objects/o:' . $document['pid'] . '/methods/bdef:Asset/getThumbnail  "></td>';
	echo "<br>";
	
	

	echo "<td>Oggetto:"."<td>" . $document2['res'] . "<td>".'<img src="http://fc.cab.unipd.it/fedora/objects/o:' . $rest2 . '/methods/bdef:Asset/getThumbnail  ">';
	
	
	



	
	
	
	
	

	echo "</tr>";
}
echo "</table>";
echo "</pre>";



$riga2=$document2['res'];
$inizio2 = strpos($riga2, 'http://phaidra.cab.unipd.it/o:');



$nuova_riga2 = substr($riga2, $inizio2);
$rest2 = substr($nuova_riga2, 30, 5);



//echo $nuova_riga;
//echo $rest;



	



}





