<?php 

$riga = '{"metadata":{"alerts":[],"dc":[{"ui_value":"All rights reserved","xmlname":"rights"},{"ui_value":"Museo Botanico, cassettiera","xmlname":"source"},{"ui_value":"ita","xmlname":"language"},{"ui_value":"Canella, Renzo","xmlname":"creator"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"Botanica, fusto","xmlname":"subject"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"Biologia vegetale. Tav.V. Fig.1. Sezione longitudinale radiale di un vaso di Quercia (Quercus robur) in corrispondenza di un raggio midollare. Fig.2. Porzione di fusto di Pino (Pinus silvestris) Conifere. Fig.3. Porzione di fusto di Quercia (Quercus robur) Corilacee. Fig.4. A) Sezione trasversale di tracheide di Pino (Pinus silvestris); B) Sezione longitudinale radiale in corrispondenza di una punteggiatura areolata; C) Sezione longitudinale tangenziale colla punteggiatura tagliata per traverso. Fig.5. Sezione trasversale di un fusto di Aristolochia (Aristolochia sipho)Aristolochiacee. Fig.6. Sezione radiale di un fusto di Castagno (Castanea sativa)Corilacee.","xmlname":"description"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"Pulito nel 2014","xmlname":"description"},{"ui_value":"hdl:11168/11.61984","xmlname":"identifier"},{"ui_value":"http://phaidra.cab.unipd.it/o:61984","xmlname":"identifier"},{"ui_value":"image/jpeg (1682123 bytes)","xmlname":"format"},{"ui_value":"wallchart (depth: 96,5 cm, width: 66 cm)","xmlname":"format"},{"ui_value":"http://phaidra.cab.unipd.it/o:62504","xmlname":"relation"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"I fusti","xmlname":"title"},{"attributes":[{"ui_value":"eng","xmlname":"xml:lang"}],"ui_value":"Image","xmlname":"type"},{"ui_value":"G.B.Paravia & C.","xmlname":"publisher"},{"ui_value":"PHAIDRA University of Padova","xmlname":"publisher"},{"ui_value":"Barollo, Michele (Digitiser)","xmlname":"contributor"},{"ui_value":"Citon, Simone (Digitiser)","xmlname":"contributor"}],"status":200}}"';
$inizio = strpos($riga, 'http://phaidra.cab.unipd.it/o:');



$nuova_riga = substr($riga, $inizio);
$rest = substr($nuova_riga, 30, 5);



//echo $nuova_riga;
echo $rest;





